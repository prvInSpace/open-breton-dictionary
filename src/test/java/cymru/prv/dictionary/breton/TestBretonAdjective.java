package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

public class TestBretonAdjective {

    @Test
    public void testDefaultInflections(){
        var adj = new BretonAdjective(
                new JSONObject().put("normalForm", "ruz"));
        var inflections = adj.toJson().getJSONObject("inflections");
        Assertions.assertIterableEquals(List.of("ken ruz"), Json.getStringList(inflections, "equative"));
        Assertions.assertIterableEquals(List.of("rusoc'h"), Json.getStringList(inflections, "comparative"));
        Assertions.assertIterableEquals(List.of("rusañ"), Json.getStringList(inflections, "superlative"));
        Assertions.assertIterableEquals(List.of("rusat"), Json.getStringList(inflections, "exclamative"));
    }

}
