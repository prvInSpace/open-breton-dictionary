package cymru.prv.dictionary.breton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BretonMascNumber {

    @Test
    public void test1(){
        var converter = new BretonMascNumberToText();
        Assertions.assertEquals("unan", converter.convertToText(1));
    }

    @Test
    public void test26(){
        Assertions.assertEquals(
                "c'hwec'h warn-ugent",
                new BretonMascNumberToText().convertToText(26)
        );
    }

    @Test
    public void testMax(){
        Assertions.assertDoesNotThrow(
                () -> new BretonMascNumberToText().convertToText(Long.MAX_VALUE)
        );
    }

}
