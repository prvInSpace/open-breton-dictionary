package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.DictionaryList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestBretonDictionary {

    @Test
    public void testIsAbleToInitialize(){
        Assertions.assertDoesNotThrow(
                () -> new BretonDictionary()
        );
    }

    @Test
    public void testIsAbleToInitializeWithList(){
        Assertions.assertDoesNotThrow(() -> {
            DictionaryList list = new DictionaryList();
            BretonDictionary dictionary = new BretonDictionary(list);
            Assertions.assertNotEquals(0, dictionary.getNumberOfWords());
            Assertions.assertNotEquals(0, list.getNumberOfAmbiguousWords());
        });
    }

    @Test
    public void testVersionRedirectWords(){
        var dictionary = new BretonDictionary();
        var words = dictionary.getWords("moned");
        Assertions.assertEquals(1, words.size());
        Assertions.assertEquals("mont", words.get(0).getNormalForm());
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new BretonDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }

}
