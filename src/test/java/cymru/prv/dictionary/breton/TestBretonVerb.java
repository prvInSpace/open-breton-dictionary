package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;


/**
 * @author Preben Vangberg
 */
public class TestBretonVerb {

    @Test
    public void testConjugatesFlagSetToFalse(){
        var word = new BretonVerb(
                new JSONObject().put("normalForm", "test").put("conjugates", false));
        Assertions.assertNull(word.getConjugations());
    }

    @Test
    public void testWordWithoutOverrideForPastParticipleShouldGenerateIt(){
        var word = new BretonVerb(
                new JSONObject().put("normalForm", "testout"));
        assertIterableEquals(List.of("testet"), Json.getStringList(word.toJson(), "past_participle"));
    }

    @Test
    public void testWordWithOverrideForPastParticipleShouldExportIt(){
        var word = new BretonVerb(
                new JSONObject().put("normalForm", "testout").put("past_participle", "my test"));
        assertIterableEquals(List.of("my test"), Json.getStringList(word.toJson(), "past_participle"));
    }

}
