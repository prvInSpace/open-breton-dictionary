package cymru.prv.dictionary.breton;

/**
 * A number to feminine number converter
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see BretonMascNumberToText
 */
public class BretonFemNumberToText extends BretonMascNumberToText {

    @Override
    protected String getSubTwenty(long number) {
        number %= 20;
        if(number == 2)
            return "div";
        if(number == 3)
            return "teir";
        if(number == 4)
            return "peder";
        return super.getSubTwenty(number);
    }
}
