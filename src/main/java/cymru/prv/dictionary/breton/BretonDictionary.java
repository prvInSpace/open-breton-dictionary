package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;


/**
 * Represent a Breton dictionary.
 * Creates a dictionary with the language code "br"
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Dictionary
 */
public class BretonDictionary extends Dictionary {

    private static final Map<WordType, Function<JSONObject, Word>> wordTypeToJsonMap = Map.of(
            WordType.verb, BretonVerb::new,
            WordType.noun, BretonNoun::new,
            WordType.adjective, BretonAdjective::new,
            WordType.adposition, BretonAdposition::new,
            WordType.conjunction, (w) -> new Word(w, WordType.conjunction)
    );

    public BretonDictionary(){
        super("br", wordTypeToJsonMap);
    }

    public BretonDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, "br", wordTypeToJsonMap);
    }

    @Override
    public String getVersion(){
        return "1.2.0";
    }

}
