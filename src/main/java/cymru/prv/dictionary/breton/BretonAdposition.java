package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Inflection;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.List;


/**
 * Represents a Breton adposition
 * <p>
 * For inflected adpositions it contains an instance of
 * Inflection. Otherwise it only contains the same
 * information as BretonWord
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Inflection
 */
public class BretonAdposition extends Word {

    private final Inflection inflection;

    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @param dictionary The dictionary the word is in
     * @param obj        The data for the word
     * @throws IllegalArgumentException if normalForm is empty or null
     */
    public BretonAdposition(JSONObject obj) {
        super(obj, WordType.adposition);
        inflection = obj.has("inflected") ? new Inflection(obj) : null;
    }


    /**
     * If the adposition is inflected it
     * will return the information in
     * the inflection object. Otherwise
     * it will return null.
     *
     * @return the inflection if it exists,
     *         otherwise null
     */
    @Override
    protected JSONObject getInflections() {
        if(inflection != null)
            return inflection.toJson();
        return null;
    }


    /**
     * Returns the default versions and
     * the versions in the inflection if
     * it exists.
     *
     * @return the different version of the word
     */
    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(inflection != null)
            list.addAll(inflection.getVersions());
        return list;
    }
}
