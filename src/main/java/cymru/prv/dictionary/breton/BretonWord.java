package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

/**
 * Represents a Breton word
 * <p>
 * Common for all Breton words is that they can
 * mutate. Therefore the mutation takes place on
 * this level.
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Word
 */
public class BretonWord extends Word {

    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @param obj        The data for the word
     * @param type       The word type of the word
     * @throws IllegalArgumentException if normalForm is empty or null
     */
    public BretonWord(JSONObject obj, WordType type) {
        super(obj, type);
    }


    /**
     * Fetches the mutations for the normal form
     * of the word.
     *
     * @return the mutations of the normal-form as a JSONObject
     */
    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(getNormalForm());
    }

}
