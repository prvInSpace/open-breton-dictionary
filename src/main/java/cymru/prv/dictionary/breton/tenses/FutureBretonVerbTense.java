package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * Represents the future tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class FutureBretonVerbTense extends BretonVerbTense {

    public FutureBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Arrays.asList(apply("in"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply("i"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Arrays.asList(apply("o"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Arrays.asList(apply("imp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("ot"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Arrays.asList(apply("int"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Arrays.asList(apply("or"));
    }
}
