package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the conditional present tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class ConditionalPresentBretonVerbTense extends BretonVerbTense {

    public ConditionalPresentBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("fen"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("fes"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply("fe"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("femp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("fec'h"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("fent"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(apply("fed"));
    }
}
