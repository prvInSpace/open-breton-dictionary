package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Represents the imperfect tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class ImperativeBretonVerbTense extends BretonVerbTense {

    public ImperativeBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply(""));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Arrays.asList(apply("et"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Arrays.asList(apply("omp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("it"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Arrays.asList(apply("ent"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.emptyList();
    }
}
