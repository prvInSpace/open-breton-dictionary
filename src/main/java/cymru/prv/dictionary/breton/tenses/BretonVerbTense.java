package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import cymru.prv.dictionary.common.Conjugation;
import org.json.JSONObject;


/**
 * Abstract class that represents a general tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public abstract class BretonVerbTense extends Conjugation {

    private final String stem;

    public BretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", verb.getStem());
    }

    protected String apply(String suffix) {
        return stem + suffix;
    }

}
