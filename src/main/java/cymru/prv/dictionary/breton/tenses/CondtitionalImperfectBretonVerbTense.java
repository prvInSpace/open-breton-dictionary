package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the conditional imperfect tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CondtitionalImperfectBretonVerbTense extends BretonVerbTense {

    public CondtitionalImperfectBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("jen"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("jes"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply("je"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("jemp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("jec'h"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("jent"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(apply("jed"));
    }
}
