package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the preterite tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class PreteriteBretonVerbTense extends BretonVerbTense {

    public PreteriteBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("is"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("jout"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply("as"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("jomp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("joc'h"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("jont"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(apply("jod"));
    }
}
